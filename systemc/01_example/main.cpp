#include <iostream>
#include <systemc>

int sc_main(int argc, char *argv[])
{
  sc_core::sc_start(1, sc_core::SC_MS);
  std::cout << "Wow! I am able to link to SystemC package via conan + cmake." << std::endl;
	return 0;
}

