This is a simple setup for creating a systemc binary using [systemc/2.3.3 prebuilt library](https://conan.io/center/systemc) from [conan](https://conan.io/)

## How to create binary and run ? 
- **On Windows :**
  - Debug version   : mkdir build && cd build && conan install -pr=../conan/vs2019_x86_64_debug .. && conan build ..  
  - Release version : mkdir build && cd build && conan install -pr=../conan/vs2019_x86_64_release .. && conan build ..
- **On WSL/Linux :**
  - mkdir build && cd build && conan install -pr=../conan/gcc10_x86_64_linux_debug .. && conan build .. && ./main 


