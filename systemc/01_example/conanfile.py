from conans import ConanFile, CMake, tools


class SystemC_Example_01(ConanFile):
    requires = "systemc/2.3.3"
    generators = ["cmake_find_package","cmake_paths"]
    settings = "os", "compiler", "build_type", "arch"
    name = "SystemC_Example_01"
    version = "1.0.0"

    def build(self):
      cmake = CMake(self) 
      cmake.configure()
      cmake.build()
